from Magellan import *
import random

if __name__ == '__main__':
    magel = Magellan()
    magel.begin('/dev/ttyUSB0', 9600)

    print('------------------------ post ----------------------')

    while True:
        for x in range(1):
            number = random.randint(0, 100)
            print("Random number:", number)

            magel.post('594cdcd0-1eee-11e9-b64a-754d850ffc63', \
                           '{"Random number":' + str(number) + '}')

        # magel.post('594cdcd0-1eee-11e9-b64a-754d850ffc63', \
        #            '{"temperature":' + '9999' + \
        #            ',' + '"cputemp":' + '9999' + \
        #            ',' + '"cpuvolt":' + '9999' + \
        #            ',' + '"cpuusage":' + '9999' + \
        #            ',' + '"freeram":' + '9999' + \
        #            ',' + '"remdisk":' + '9999' + \
        #            ',' + '"batterry":' + '9999' + \
        #            ',' + '"cputemp1":' + '9999' + \
        #            ',' + '"cpuvolt1":' + '9999' + \
        #            ',' + '"cpuusage1":' + '9999' + \
        #            ',' + '"freeram1":' + '9999' + \
        #            ',' + '"remdisk1":' + '9999' + \
        #            ',' + '"batterry1":' + '9999' + \
        #            ',' + '"cputemp2":' + '3919' + \
        #            ',' + '"cpuvolt2":' + '3299' + \
        #            ',' + '"cpuusage2":' + '3993' + \
        #            ',' + '"freeram2":' + '3994' + \
        #            ',' + '"remdisk2":' + '3599' + \
        #            ',' + '"batterry2":' + '3996' + \
        #            ',' + '"cputemp3":' + '3997' + \
        #            ',' + '"cpuvolt3":' + '3998' + \
        #            ',' + '"cpuusage3":' + '3999' + \
        #            ',' + '"freeram3":' + '4990' + \
        #            ',' + '"remdisk3":' + '4991' + \
        #            ',' + '"batterry3":' + '499112' + \
        #            ',' + '"cputemp4":' + '319911' + \
        #            ',' + '"loc":[13.7464738,100.53456499999993]' + '}')

        time.sleep(5)

    magel.close()