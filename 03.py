from Magellan import *
import psutil

if __name__ == '__main__':

    magel = Magellan()
    magel.begin('/dev/ttyUSB0', 9600)

    while True:
        cpuAll = str("%.2f" %psutil.cpu_percent())
        cpuPercore = psutil.cpu_percent(percpu=True)
        cpu0 = str("%.2f" %cpuPercore[0])
        cpu1 = str("%.2f" %cpuPercore[1])
        cpu2 = str("%.2f" %cpuPercore[2])
        cpu3 = str("%.2f" %cpuPercore[3])
        cpuf = str("%.2f" %psutil.cpu_freq()[0])
        ram = str("%.2f" %psutil.virtual_memory()[2])
        
        magel.post('594cdcd0-1eee-11e9-b64a-754d850ffc63',\
            '{"CPU Usages":' + cpuAll + \
            ',' + '"CPU Core1":' + cpu0 + \
            ',' + '"CPU Core2":' + cpu1 + \
            ',' + '"CPU Core3":' + cpu2 + \
            ',' + '"CPU Core4":' + cpu3 + \
            ',' + '"CPU frequency":' + cpuf + \
            ',' + '"Memory Usages":' + ram + '}')

        time.sleep(5)
    magel.close()