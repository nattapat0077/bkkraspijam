from Magellan import *
from subprocess import PIPE, Popen
import os
import psutil
import random
import time

def get_cpu_temperature():
    """get cpu temperature using vcgencmd"""
    process = Popen(['vcgencmd', 'measure_temp'], stdout=PIPE)
    output, _error = process.communicate()
    cputemp=output.decode('utf8')
    fcputemp=float(cputemp[cputemp.index('=')+1:cputemp.rindex("'")])
    return fcputemp

def get_cpu_voltcore():
    """get cpu volt core using vcgencmd"""
    process = Popen(['vcgencmd', 'measure_volts core'], stdout=PIPE)
    output, _error = process.communicate()
    cpuvolt=output.decode('utf8')
    fcpuvolt=float(cpuvolt[cpuvolt.index('=')+1:cpuvolt.rindex("V")])
    #print(fcpuvolt)
    return fcpuvolt

# Return RAM information (unit=kb) in a list
# Index 0: total RAM
# Index 1: used RAM
# Index 2: free RAM
def getRAMinfo():
    p = os.popen('free')
    i = 0
    while 1:
        i = i + 1
        line = p.readline()
        if i==2:
            return(line.split()[1:4])

# Return % of CPU used by user as a character string
def getCPUuse():
    return(str(os.popen("top -n1 | awk '/Cpu\(s\):/ {print $2}'").readline().strip()))

# Return information about disk space as a list (unit included)
# Index 0: total disk space
# Index 1: used disk space
# Index 2: remaining disk space
# Index 3: percentage of disk used
def getDiskSpace():
    p = os.popen("df -h /")
    i = 0
    while 1:
        i = i +1
        line = p.readline()
        if i==2:
            return(line.split()[1:5])

if __name__ == '__main__':

    
    magel=Magellan()
    magel.begin('/dev/ttyUSB0',9600,0)
    
    get_cpu_temperature()
    
    while True:

        temperature=str(random.randrange(23,26))
        cputemp=str(get_cpu_temperature())
        cpuvolt=str(get_cpu_voltcore())
        cpuusage=str(psutil.cpu_percent(interval=1))
        ram=str(getRAMinfo()[2])
        Disk=str(getDiskSpace()[3].replace('%',''))
        batt="80"
        print('------------------------ post ----------------------')

        magel.post('594cdcd0-1eee-11e9-b64a-754d850ffc63',\
                   '{"temperature":'+cputemp+\
                   ','+'"cputemp":'+cputemp+\
                   ','+'"cpuvolt":'+cpuvolt+\
                   ','+'"cpuusage":'+cpuusage+\
                   ','+'"freeram":'+ram+\
                   ','+'"remdisk":'+Disk+\
                   ','+'"batterry":'+batt+\
                   ','+'"loc":[13.7464738,100.53456499999993]'+ '}')

        time.sleep(5)
    magel.close()

    
