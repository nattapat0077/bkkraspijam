# External module imports
import RPi.GPIO as GPIO
import time

# Pin Definitons:
ledPin = 16 # Broadcom pin 23 (P1 pin 16)
butPin = 20 # Broadcom pin 17 (P1 pin 20)

# Pin Setup:
GPIO.setmode(GPIO.BCM) # Broadcom pin-numbering scheme
GPIO.setup(ledPin, GPIO.OUT) # LED pin set as output
GPIO.setup(butPin, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Button pin set as input w/ pull-up

# Initial state for LEDs:
GPIO.output(ledPin, GPIO.LOW)
print('0')

print("Here we go! Press CTRL+C to exit")
try:
    while 1:
        if GPIO.input(butPin): # button is released
            GPIO.output(ledPin, GPIO.LOW)
            print('0')
        else: # button is pressed
            GPIO.output(ledPin, GPIO.HIGH)
            print('1')
except KeyboardInterrupt: # If CTRL+C is pressed, exit cleanly:
    GPIO.cleanup() # cleanup all GPIO
