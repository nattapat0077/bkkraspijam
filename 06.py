import RPi.GPIO as GPIO
from Magellan import *
import time
import random
import Adafruit_DHT
from subprocess import PIPE, Popen
import os
import psutil

GPIO.setmode(GPIO.BCM)

# set GPIO Pins
GPIO_TRIGGER = 18
GPIO_ECHO = 24

# set GPIO direction (IN / OUT)
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)


def distance():
    GPIO.output(GPIO_TRIGGER, True)

    time.sleep(0.00001)
    GPIO.output(GPIO_TRIGGER, False)

    StartTime = time.time()
    StopTime = time.time()

    while GPIO.input(GPIO_ECHO) == 0:
        StartTime = time.time()

    while GPIO.input(GPIO_ECHO) == 1:
        StopTime = time.time()

    TimeElapsed = StopTime - StartTime
    distance = (TimeElapsed * 34300) / 2

    return distance

def get_cpu_temperature():
    """get cpu temperature using vcgencmd"""
    process = Popen(['vcgencmd', 'measure_temp'], stdout=PIPE)
    output, _error = process.communicate()
    cputemp=output.decode('utf8')
    fcputemp=float(cputemp[cputemp.index('=')+1:cputemp.rindex("'")])
    return fcputemp

def getCPUuse():
    return(str(os.popen("top -n1 | awk '/Cpu\(s\):/ {print $2}'").readline().strip()))

if __name__ == '__main__':

    magel = Magellan()
    magel.begin('/dev/ttyUSB0', 9600)

    print('------------------------ post ----------------------')

    try:
        while True:
            batt = 100
            while batt >= 0:
                dist = distance()
                humidity, temperature = Adafruit_DHT.read_retry(11, 8)
                number = random.randint(0, 100)

                cputemp = get_cpu_temperature()
                cpuusage = psutil.cpu_percent(interval=1)

                if (batt == 0):
                    break
                else:
                    if humidity is not None and temperature is not None:
                        print('Batterry = ', batt)
                        print('Random Number = ', number)
                        print('Measured Distance = %.1f cm' % dist)
                        print('Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(temperature, humidity))

                        magel.post('594cdcd0-1eee-11e9-b64a-754d850ffc63', \
                                   '{"batterry":' + str("%d" % batt) + \
                                   ',' + '"measured distance":' + str("%.2f" %dist) + \
                                   ',' + '"temp":' + str(temperature) + \
                                   ',' + '"humidity":' + str(humidity) + \
                                   ',' + '"cputemp":' + str(cputemp) + \
                                   ',' + '"cpuusage":' + str(cpuusage) + \
                                   ',' + '"loc":[13.7601996,100.5693467]' + \
                                   ',' + '"random":' + str(number) + '}')

                        batt -= 0.01
                        time.sleep(5)

                    else:
                        print('Failed to get reading. Try again!')

    # Reset by pressing CTRL + C
    except KeyboardInterrupt:
        print("Measurement stopped by User")
        GPIO.cleanup()
        magel.close()