from Magellan import *
import time
import random

if __name__ == '__main__':

    magel = Magellan()
    magel.begin('/dev/ttyUSB0', 9600)

    print('------------------------ post ----------------------')

    while True:
        batt = 100

        while batt >= 0:
            number = random.randint(0, 100)
            if (batt == 0):
                break
            else:
                print(number, "%.2f"%batt)
                magel.post('594cdcd0-1eee-11e9-b64a-754d850ffc63', \
                   '{"batterry":' + str("%d" %batt) + \
                   # '{"batterry":' + str(batt) + \
                   ',' + '"random":' + str(number) + '}')

            batt -= 0.01
            time.sleep(5)

    magel.close()